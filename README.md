Test data for Taguette document conversion
==========================================

This repository uses [git-lfs](https://git-lfs.github.com/). You can run `git lfs pull` to get the files.

See also [the main Taguette repository](https://gitlab.com/remram44/taguette).
